package com.ssbanks;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Transactions {
	
	private double currentAcountBallance;
	private double transAmnt;
	private Date transactionDate;
	private String transType;
	
	public Transactions(double currentAcountBallance, Date transactionDate, 
							String transType) {
		this.setTransAmnt(currentAcountBallance);
		this.transactionDate = transactionDate;
		this.transType = transType;
		this.currentAcountBallance = currentAcountBallance;
	}
	
	public Transactions(double ammount, Date transactionDate, String transType, 
							double newBal) {
		this.setTransAmnt(ammount);
		this.transactionDate = transactionDate;
		this.transType = transType;
		this.currentAcountBallance = newBal;
	}

	public double getCurrentAcountBallance() {
		return currentAcountBallance;
	}

	public String getTransactionDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return dateFormat.format(transactionDate);
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}
	
	public void setCurrentAcountBallance(double currentAcountBallance) {
		this.currentAcountBallance = currentAcountBallance;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public double getTransAmnt() {
		return transAmnt;
	}

	public void setTransAmnt(double transAmnt) {
		this.transAmnt = transAmnt;
	}
}
