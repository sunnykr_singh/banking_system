/**
 * this is a base class of banking system
 * it performs all the user request such as
 * withdraw, deposit, and providing transaction history
 * this class has a list of customers
 */

package com.ssbanks;

import java.util.ArrayList;
import java.util.Date;

public class Branch {
	private String name;
	private ArrayList<Customer> customers;
	
	public Branch(String name) {
		this.setName(name);
		this.customers = new ArrayList<Customer>();
	}
	
	public String getName() {
		return name;
	}

	public ArrayList<Customer> getCustomers() {
		return BankDAO.getCustomerList(this);
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean newCustomer(String name, double acBal, String pass) {
		String acNo = generateAccountNumber();
		if(acNo != null) {
			Customer c = new Customer(name, acNo, acBal, pass);
			//customers.add(new Customer(name, acNo, acBal, pass));
			BankDAO.addCustomer(c,getName());
			BankDAO.addTransaction(c.getAcNumber(), new Transactions(c.getAcBalance(), new Date(), "cr"));
			if(findCustomer(acNo)!=null) {
				System.out.println("Welcome" + name);
				System.out.println("Your Account Number is "+acNo);
				return true;
			}
		}
		System.out.println("Sorry we are facing some issue\nPlease try again leter");
		return false;
	}
	
	private String generateAccountNumber() {
		boolean unique = false;
		String acNumber = null;
		while(!unique) {
			long number = (long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L;
			acNumber = Long.toString(number);
			if(findCustomer(acNumber) == null) {
				unique = true;
			}
		}
		return acNumber;
	}
	
	public ArrayList<Transactions> trasactionDetails(String acNumber) {
		Customer account = findCustomer(acNumber);
		if(account != null) {
			return account.getTransactions(); 
		}else {
			System.out.println("Account Not Found");
			return null;
		}
	}
	
	public boolean depositMoney(String acNumber, double ammount) {
		Customer account = findCustomer(acNumber);
		if(account != null) {
			double crrBal = account.getAcBalance();
			double newBal = crrBal + ammount;
			account.setAcBallance(newBal);
			account.setTransactions(ammount, new Date(), "cr", newBal);
			System.out.println(ammount+" credited to your account\nNew Account Banlance is "+newBal);
			return true;
		}else {
			System.out.println("Account Not Found");
		}
		return false;
	}
	
	public boolean withdrawMoney(String acNumber, String password, double ammount) {
		Customer account = findCustomer(acNumber);
		if(account != null) {
			if(Validation.verify(account, password)) {
				double crrBal = account.getAcBalance();
				if(crrBal >= ammount) {
					double newBal = crrBal - ammount;
					account.setAcBallance(newBal);
					account.setTransactions(ammount, new Date(), "dr", newBal);
					System.out.println("Please Collect Cash\nLeft Account Balance is "+newBal);
					return true;
				}else {
					System.out.println("Not enough Ballance");
				}
			}else {
				System.out.println("Incorrect Password");
			}
		}else {
			System.out.println("Account Not Found");
		}
		return false;
	}
	
	public boolean changePassword(String acNumber, String oldPass, String newPass) {
		Customer account = findCustomer(acNumber);
		if(account != null) {
			if(Validation.verify(account, oldPass)) {
				account.setPassword(newPass);
				System.out.println("Password changed successfully");
			}else {
				System.out.println("Incorrect Password");
			}
		}else {
			System.out.println("Account Not Found");
		}
		return false;
	}
	
	private Customer findCustomer(String acNumber) {
		customers = BankDAO.getCustomerList(this);
		if(customers!=null) {
			for(Customer c : customers) {
				if(acNumber.equals(c.getAcNumber()))
					return c;
			}
		}
		return null;
	}
}
