/**
 * Bank main
 * this file contains main() for Bank project and
 * other methods to interact with user
 */

package com.ssbanks;

import java.util.InputMismatchException;
import java.util.Scanner;

public class BankMain {
	
	static Scanner read = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		Bank bank = new Bank("S.S. Bank");
		
		mainMenu(bank);
	}
	
	// mainMenu to be printed on first screen
	private static void mainMenu(Bank bank) {
		boolean quit = false;
		
		while(!quit) {
			System.out.println("---------------------------------------------");
			System.out.println("1. New User Create Account\n" +
							   "2. Existing User\n" +
							   "3. Administrator\n" +
							   "4. Exit");
			System.out.println("---------------------------------------------");
			int option;
			try {
				option = read.nextInt();
				read.nextLine();
				switch(option) {
				case 1:
					processAddingCustomer(bank);
					break;
				case 2:
					exeistingCustomer(bank);
					break;
				case 3:
					administrator(bank);
					break;
				case 4:
					System.out.println("Thank You for using our services");
					System.exit(0);
				default:
					System.out.println("Invalid Option");
				}
			}catch(InputMismatchException e) {
				System.out.println("Invalid Input\nPlease try again");
				System.exit(0);
		}
		}
	}

	private static void processAddingCustomer(Bank bank) {
		System.out.print("Enter Branch Name : ");
		String branchName = read.nextLine();
		System.out.print("Enter Your Name\t: ");
		String name = read.nextLine();
		System.out.print("Set Password\t: ");
		String password = read.nextLine();
		System.out.print("Intial Ammount\t: ");
		double ammount = read.nextDouble();
		bank.addCustomer(branchName, name, ammount, password);
		mainMenu(bank);
	}
	
	//menu for existing customers
	private static void exeistingCustomer(Bank bank) {
		
		System.out.print("Enter Branch Name : ");
		String branchName = read.nextLine();
		System.out.print("Account Number\t: ");
		String acNumber = read.nextLine();
		
		boolean quit = false;
		while(!quit) {
			System.out.println("---------------------------------------------");
			System.out.println("1. Withdraw Money\n" + 
							   "2. Deposit Money\n" +
							   "3. Mini Statement\n" +
							   "4. Change Password\n" +
							   "5. Exit to Main Menu\n" +
							   "6. Exit from Bank");
			System.out.println("---------------------------------------------");
			int option = read.nextInt();
			read.nextLine();
			quit = processRequest(option, bank, branchName, acNumber);
		}
	}
	
	//process existing customer's request
	private static boolean processRequest(int option, Bank bank, String branchName,
									String acNumber) {
		double ammount;
		String password;
		
		switch(option) {
		case 1:
			System.out.print("Enter Password\t: ");
			password = read.nextLine();
			System.out.print("Enter Ammount\t: ");
			ammount = read.nextDouble();
			read.nextLine();
			bank.withdrawMoney(branchName, acNumber, password, ammount);
			break;
		case 2:
			System.out.print("Enter Ammount\t: ");
			ammount = read.nextDouble();
			read.nextLine();
			bank.depositMoney(branchName, acNumber, ammount);
			break;
		case 3:
			bank.getTrasactionDetails(branchName, acNumber);
			break;
		case 4:
			System.out.print("Enter Old Password\t: ");
			password = read.nextLine();
			System.out.print("Enter New Password\t: ");
			String newPassword = read.nextLine();
			bank.changePassword(branchName, acNumber, password, newPassword);
			break;
		case 5:
			return true;
		case 6:
			System.out.println("Thank You for using our services");
			System.exit(0);
		default:
			System.out.println("Invalid Option");
		}
		return false;
	}
	
	private static void administrator(Bank bank) {
		boolean quit = false;
		while(!quit) {
			System.out.println("---------------------------------------------");
			System.out.println("1. Create Branch\n" +
							   "2. List All Branches\n" +
							   "3. List All Customer of a Branch\n" +
						   	   "4. Exit to Main Menu\n" +
							   "5. Exit from Bank");
			System.out.println("---------------------------------------------");
			int option = read.nextInt();
			read.nextLine();
			quit = processAdminRequest(option,bank);
		}
	}

	private static boolean processAdminRequest(int option,Bank bank) {
		String branchName;
		
		switch(option) {
		case 1:
			System.out.print("Enter the Name for Branch : ");
			branchName = read.nextLine();
			bank.newBranch(branchName);
			break;
		case 2:
			bank.listOfBranches();
			break;
		case 3:
			System.out.print("Enter Branch Name : ");
			branchName = read.nextLine();
			bank.listOfCustomers(branchName);
			break;
		case 4:
			return true;
		case 5:
			System.out.println("Exiting from Bank...");
			System.exit(0);
		default:
			System.out.println("Invalid Option");
		}
		return false;
	}
	
}