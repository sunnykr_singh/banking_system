package com.ssbanks;

import java.util.ArrayList;
import java.util.Date;

public class Customer {
	private String name;
	private String acNumber;
	private double acBalance;
	private String password;
	private ArrayList<Transactions> transactions;
	
	public Customer(String name, String acNumber, double acBalance, String password) {
		this.name = name;
		this.acNumber = acNumber;
		this.acBalance = acBalance;
		this.password = password;
		this.transactions = new ArrayList<Transactions>();
		//this.transactions.add(new Transactions(acBallance, new Date(), "cr"));
		//BankDAO.addTransaction(acNumber, new Transactions(acBallance, new Date(), "cr"));
	}

	public String getName() {
		return name;
	}
	
	public String getAcNumber() {
		return acNumber;
	}
	
	public double getAcBalance() {
		return acBalance;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAcNumber(String acNumber) {
		this.acNumber = acNumber;
	}
	
	public void setAcBallance(double acBalance) {
		BankDAO.updateAccountBalance(acBalance,this);
		this.acBalance = acBalance;
	}
	
	public void setPassword(String password) {
		//this.password = password;
		BankDAO.changePassword(this, password);
	}
	
	public ArrayList<Transactions> getTransactions(){
		transactions = BankDAO.getTransactionList(this);
		return transactions;
	}
	
	public void setTransactions(double ammount, Date crrDate, String transType, double newBal){
		//this.transactions.add(new Transactions(ammount, crrDate, transType, newBal));
		BankDAO.addTransaction(this.getAcNumber(), new Transactions(ammount,crrDate, transType, newBal));
	}
}
