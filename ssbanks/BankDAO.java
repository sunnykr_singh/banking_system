package com.ssbanks;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
public class BankDAO {
	private static Connection con;
	private static Statement st;
	private static ResultSet rs;
	static Connection getConnection()throws Exception{
		Class.forName("oracle.jdbc.driver.OracleDriver");
		return DriverManager.getConnection("jdbc:oracle:thin:@localHost:1521:orcl2","scott","tiger");
	}
	
	static boolean addBranch(Branch b) {
		try {
			con = getConnection();
			st = con.createStatement();
			int val = st.executeUpdate("insert into branch values('"+b.getName()+"')");
			return (val>0?true:false);
		}catch(Exception e1) {
			e1.printStackTrace();
		}
		return false;
	}
	
	static boolean addCustomer(Customer c,String branch) {
		try {
			con = getConnection();
			st = con.createStatement();
			int val = st.executeUpdate("insert into customer values('"+c.getAcNumber()+"','"+c.getName()+
									"','"+c.getAcBalance()+"','"+c.getPassword()+"','"+branch+"')");
			return (val>0?true:false);
		}catch(Exception e1) {
			e1.printStackTrace();
		}
		return false;
	}
	
	static boolean addTransaction(String acNo, Transactions t) {
		try {
			con = getConnection();
			st = con.createStatement();
			int val = st.executeUpdate("insert into transactions values('"+t.getCurrentAcountBallance()+
						"','"+t.getTransAmnt()+"', sysdate ,'"+t.getTransType()+
						"','"+acNo+"')");
			return (val>0?true:false);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	static boolean updateAccountBalance(double newBal,Customer c) {
		try {
			con = getConnection();
			st = con.createStatement();
			int val = st.executeUpdate("update customer set acbal = '"+newBal+"' where acno = '"+c.getAcNumber()+"'");
			return (val>0?true:false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	static boolean changePassword(Customer c, String newPass) {
		try {
			con = getConnection();
			st = con.createStatement();
			int val = st.executeUpdate("update customer set pass = '"+newPass+"' where acno = '"+c.getAcNumber()+"'");
			return (val>0?true:false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	static ArrayList<Branch> getBranchList(){
		try {
			con = getConnection();
			st = con.createStatement();
			rs = st.executeQuery("select * from branch");
			if(rs!=null) {
				ArrayList<Branch> branches = new ArrayList<Branch>();
				while(rs.next()) {
					String name = rs.getString(1);
					branches.add(new Branch(name));
				}
				return branches;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	static ArrayList<Customer> getCustomerList(Branch branch) {
		try {
			con = getConnection();
			st = con.createStatement();
			rs = st.executeQuery("select * from customer where branch = '"+branch.getName()+"'");
			if(rs!=null) {
				ArrayList<Customer> customers = new ArrayList<Customer>();
				while(rs.next()) {
					String acNo = rs.getString(1);
					String name = rs.getString(2);
					double acBal = rs.getDouble(3);
					String pass = rs.getString(4);
					customers.add(new Customer(name, acNo, acBal, pass));
				}
				return customers;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;	
	}
	
	static ArrayList<Transactions> getTransactionList(Customer c) {
		try {
			con = getConnection();
			st = con.createStatement();
			rs = st.executeQuery("select * from transactions where acno = '"+c.getAcNumber()+"'");
			if(rs!=null) {
				ArrayList<Transactions> trans = new ArrayList<Transactions>();
				while(rs.next()) {
					double newBal = rs.getDouble(1);
					double ammount = rs.getDouble(2);
					Date crrDate = rs.getDate(3);
					String transType = rs.getString(4);
					trans.add(new Transactions(ammount, crrDate, transType, newBal));
				}
				return trans;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;	
	}
	
}
