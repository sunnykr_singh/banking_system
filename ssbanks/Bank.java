/**
 * This class interacts with the Bank main and branch
 * it provides functionalities of banking system to user end
 * all process will be perform at the branch class
 * this class takes users request and process it at branch level
 * then gets the result from branch and show it to user
 * 
 * this class is also responsible for admin level activity
 * this class has the list of branches
 * it is responsible to add branch and get the details of every branch
 * and their customers
 */

package com.ssbanks;

import java.util.ArrayList;

public class Bank {
	private String name;
	private ArrayList<Branch> branches;

	public Bank(String name) {
		this.name = name;
		this.branches = new ArrayList<Branch>();
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public ArrayList<Branch> getBranches() {
		branches = BankDAO.getBranchList();
		return branches;
	}
	
	public boolean newBranch(String name) {
		if(findBranch(name) == null) {
			//branches.add(new Branch(name));
			BankDAO.addBranch(new Branch(name));
			return true;
		}
		return false;
	}
	
	public boolean addCustomer(String branchName, String customerName, 
								double acBal, String pass) {
		Branch branch = findBranch(branchName);
		if(branch != null) {
			branch.newCustomer(customerName, acBal, pass);
			return true;
		}else {
			System.out.println("Branch not found");
		}
		return false;
	}
	
	public boolean depositMoney(String branchName,String acNumber, double ammount) {
		Branch branch = findBranch(branchName);
		if(branch != null) {
			return branch.depositMoney(acNumber, ammount);
		}else {
			System.out.println("Branch Not Found");
		}
		return false;
	}
	
	public boolean withdrawMoney(String branchName, String acNumber, String password,
									double ammount) {
		Branch branch = findBranch(branchName);
		if(branch != null) {
			return branch.withdrawMoney(acNumber, password, ammount);
		}else {
			System.out.println("Branch Not Found");
		}
		return false;
	}
	
	public boolean changePassword(String branchName, String acNumber, String oldPass, String newPass) {
		Branch branch = findBranch(branchName);
		if(branch != null) {
			return branch.changePassword(acNumber, oldPass, newPass);
		}else {
			System.out.println("Branch Not Found");
		}
		return false;
	}
	
	public void listOfCustomers(String branchName) {
		Branch branch = findBranch(branchName);
		if(branch != null) {
			ArrayList<Customer> customers = branch.getCustomers();
			System.out.println("-----------------------------------------------------------");
			System.out.println("Customer List of " + getName() +" "+ branch.getName());
			System.out.println("-----------------------------------------------------------");
			System.out.println("Account Number\tCustomer Name\tCurrent Balance");
			System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
			for(Customer c : customers) {
				System.out.println(c.getAcNumber() + "\t" + c.getName() + "\t" + c.getAcBalance());
			}
			System.out.println("-----------------------------------------------------------");
		}
	}
	
	public void getTrasactionDetails(String branchName, String acNumber) {
		Branch branch = findBranch(branchName);
		if(branch != null) {
			ArrayList<Transactions> trans = branch.trasactionDetails(acNumber);
			if(trans != null) {
				System.out.println("---------------------------------------------------");
				System.out.println("Mini Statement of " + acNumber);
				System.out.println("Transation Date\t Ammount  \t  Type\tCurrent Ballance");
				System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
				for(Transactions t : trans) {
					System.out.println(t.getTransactionDate() + "\t " 
										+ t.getTransAmnt() +"  \t  " 
										+ t.getTransType() + "\t" 
										+ t.getCurrentAcountBallance());
				}
				System.out.println("-----------------------------------------------------------");
			}
		}else {
			System.out.println("Branch Not Found");
		}
	}
	
	public void listOfBranches() {
		ArrayList<Branch> branches = getBranches();
		if(branches != null) {
			System.out.println("---------------------------------------------------");
			System.out.println("Branch Name");
			System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - -");
			for(Branch b : branches) {
				System.out.println(b.getName());
			}
			System.out.println("---------------------------------------------------");
		}
	}
	
	private Branch findBranch(String name) {
		branches = BankDAO.getBranchList();
		for(Branch b : branches) {
			if(name.equalsIgnoreCase(b.getName()))
				return b;
		}
		return null;
	}
	
}
